#!/bin/bash

PRIVATE_TOKEN=$GITLAB_PRIVATE_TOKEN

GIT_RELEASE_EXIST=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" $CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags | jq ".[0].name")

if [ $GIT_RELEASE_EXIST != "null" ]; then
    CURRENT_VERSION=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" $CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags | jq ".[0].name")

    # Increment versions
    major=0
    minor=0

    # break down the version number into it's components
    regex="([0-9]+).([0-9]+)"
    if [[ $CURRENT_VERSION =~ $regex ]]; then
    major="${BASH_REMATCH[1]}"
    minor="${BASH_REMATCH[2]}"
    fi

    # check paramater to see which number to increment
    minor=$(expr $minor + 1)

    # Update aplication version
    package_path=./package.json
    echo $(cat $package_path | jq '.version = '"\"${major}.${minor}\"") > $package_path
    cat $package_path | jq "."

    # Create a release
    GIT_RELEASE_DATA=$(echo '{"tag_name": "'${major}.${minor}'", "description": "Current version is: '${major}.${minor}'"}')

    curl --request POST -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags?tag_name=${major}.${minor}&message=${major}.${minor}&ref=${CI_COMMIT_REF_NAME}"

    curl --request POST -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" -d "$GIT_RELEASE_DATA" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags/${major}.${minor}/release"

else
    # Init release
    major=1
    minor=0

    # Update aplication version
    APP_VER=$(cat package.json | jq '.version = '"\"${major}.${minor}\"")
    echo "$APP_VER" > package.json
    cat package.json | jq "."

    GIT_RELEASE_DATA=$(echo '{"tag_name": "'${major}.${minor}'", "description": "Current version is: '${major}.${minor}'"}')

    curl --request POST -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags?tag_name=${major}.${minor}&message=${major}.${minor}&ref=${CI_COMMIT_REF_NAME}"

    curl --request POST -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $PRIVATE_TOKEN" -d "$GIT_RELEASE_DATA" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/repository/tags/${major}.${minor}/release"
fi
